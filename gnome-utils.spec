# Note that this is NOT a relocatable package
%define ver      0.20
%define rel      980713
%define prefix   /usr
%define buildroot /var
%define cc        egcs

Summary: GNOME utility programs
Name: gnome-utils
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/Libraries
Source: ftp://ftp.jimpick.com/pub/gnome/snap/gnome-objc/gnome-utils-SNAP-%{rel}.tar.gz
BuildRoot: %{buildroot}/tmp/gnome-utils-root
Obsoletes: gnome
Packager: Marc Ewing <marc@redhat.com>
URL: http://www.gnome.org
Docdir: %{prefix}/doc

%description
GNOME utility programs.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%changelog

* Mon Jul 13 1998 Andreas Kostyrka <andreas@ag.or.at>

- changed to work with CVS snapshots and conform to 
  rpmbuild/buildall naming conventions.

* Mon Apr 6 1998 Marc Ewing <marc@redhat.com>

- Integrate into gnome-utils CVS source tree

%prep
%setup -n gnome-utils

%build
export CC=%{cc}
export OBJC=%{cc}
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/bin/*
%{prefix}/man/*/*

%{prefix}/lib/*

%{prefix}/share/locale/*/*/*
%{prefix}/share/apps
#%{prefix}/share/gtoprc
%{prefix}/share/gnome/help/*
