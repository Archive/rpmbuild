# Note that this is NOT a relocatable package
%define ver      0.14
%define rel      1
%define prefix   /usr
%define buildroot /var

Summary: Enlightenment Window Manager
Name: enlightenment
Version: %ver
Release: %rel
Copyright: GPL
Group: X11/Applications
Source: ftp://www.rasterman.com/pub/enlightenment/e-0.14.1.tar.gz
Obsoletes: enl
BuildRoot: %{buildroot}/tmp/e-root
Packager: The Rasterman <raster@redhat.com>
URL: http://www.rasterman.com/
Docdir: %{prefix}/doc

%description
Enlightenment is a Windowmanager for X-Windows that is designed to be
powerful, extensible, configurable and able to be really good looking.

%changelog

* Tue Jun 2 1998 The Rasterman <raster@redhat.com>

- wrote .spec file

%prep
%setup -n e/src

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CC="egcs" CFLAGS="-O2" ./autogen.sh --prefix=%prefix
else
  CC="egcs" CFLAGS="-O2" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/usr/bin
PREFIX=$RPM_BUILD_ROOT%{prefix} make -e install
ln -sf %{prefix}/enlightenment/bin/enlightenment $RPM_BUILD_ROOT%{prefix}/bin/enlightenmento

%clean
rm -rf $RPM_BUILD_ROOT

%post

%postun

%files
%defattr(-, root, root)
%{prefix}/enlightenment/bin/enlightenment
%{prefix}/bin/enlightenment
%{prefix}/enlightenment/config

%doc AUTHORS
%doc COPYING
%doc INSTALL
%doc ChangeLog
%doc NEWS
%doc README
%doc FAQ
%doc TODO
