# Note that this is NOT a relocatable package
%define ver      0.20
%define rel      980712
%define prefix   /usr
%define buildroot /var
%define cc        egcs

Summary: GNOME core programs
Name: gnome-core
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/Libraries
Source0: ftp://ftp.jimpick.com/pub/gnome/snap/gnome-core/gnome-core-SNAP-%{rel}.tar.gz
Patch0: gnome-core-SNAP-nodocbook.patch
BuildRoot: %{buildroot}/tmp/gnome-core-root
Obsoletes: gnome
Packager: Marc Ewing <marc@redhat.com>
URL: http://www.gnome.org
Prereq: /sbin/install-info
Docdir: %{prefix}/doc

%description
Basic programs and libraries that are virtually required for
any GNOME installation.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%package devel
Summary: GNOME core libraries, includes, etc
Group: X11/Libraries
Requires: gnome-core
PreReq: /sbin/install-info

%description devel
Panel libraries and header files.

%changelog

* Mon Jun 23 1998 Andreas Kostyrka <andreas@ag.or.at>

- customized for Jim Pick's CVS snapshots.
- added applet includes.

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>

- Integrate into gnome-core CVS source tree

%prep
%setup0 -n gnome-core
%patch0 -p1

%build
export CC=%{cc}
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/bin/*
%{prefix}/lib/lib*.so.*
%{prefix}/share/*

%files devel
%defattr(-, root, root)

%{prefix}/lib/lib*.so
%{prefix}/lib/*a
%{prefix}/include/*
