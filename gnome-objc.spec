# Note that this is NOT a relocatable package
%define ver      0.20
%define rel      980712
%define prefix   /usr
%define buildroot /var
%define cc        egcs

Summary: GNOME Objective C libraries
Name: gnome-objc
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/gnome
Source0: ftp://ftp.jimpick.com/pub/gnome/snap/gnome-objc/gnome-objc-SNAP-%{rel}.tar.gz
BuildRoot: %{buildroot}/tmp/gnome-objc-root
Requires: gnome-libs
Packager: Marc Ewing <marc@redhat.com>
URL: http://www.gnome.org/
Docdir: %{prefix}/doc

%description
Basic libraries you must have installed to use GNOME programs
that are built with Objective C.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%package devel
Summary: Libraries, includes, etc to develop Objective C GNOME applications
Group: X11/gnome
Requires: gnome-objc

%description devel
Libraries, include files, etc you can use to develop Objective C
GNOME applications.

%changelog

* Mon Jun 23 1998 Andreas Kostyrka <andreas@ag.or.at>

- customized for Jim Pick's CVS snapshots.

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>

- Integrate into gnome-objc source tree

%prep
%setup -n gnome-objc

%build
export CC=%{cc}
export OBJC=%{cc}
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%post 
if ! grep %{prefix}/lib /etc/ld.so.conf > /dev/null ; then
  echo "%{prefix}/lib" >> /etc/ld.so.conf
fi

/sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/lib/lib*.so.*

%files devel
%defattr(-, root, root)

%{prefix}/lib/lib*.so
%{prefix}/lib/*a
%{prefix}/lib/*.sh
%{prefix}/include/*
