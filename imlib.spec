# Note that this is NOT a relocatable package
%define ver      1.6
%define rel      980712
%define prefix   /usr
%define buildroot /var
%define cc        egcs -g

Summary: Image loading and rendering library for X11R6
Name: imlib
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/Libraries
Source0: ftp://ftp.jimpick.com/pub/gnome/snap/imlib/imlib-SNAP-%{rel}.tar.gz
Patch0: imlib-noxshm.patch
Obsoletes: Imlib
BuildRoot: %{buildroot}/tmp/imlib-root
Packager: Marc Ewing <marc@redhat.com>
URL: http://www.labs.redhat.com/imlib
Requires: libpng 
Requires: libtiff 
Requires: libjpeg
Requires: zlib 
Requires: libgr-progs 
Requires: glib 
Requires: gtk+ 
Requires: giflib 
Requires: ImageMagick 
Docdir: %{prefix}/doc

%description
Imlib is an advanced replacement library for libraries like libXpm that
provides many more features with much greater flexability and
speed.

%package devel
Summary: Imlib headers, static libraries and documentation
Group: X11/Libraries
Requires: imlib
Obsoletes: Imlib

%description devel
Headers, static libraries and documentation for Imlib.

%changelog

* Mon Jun 22 1998 Andreas Kostyrka <andreas@ag.or.at>

- customized for Jim Pick's CVS snapshots.

* Fri Apr 3 1998 Michael K. Johnson <johnsonm@redhat.com>

- fixed typo

* Fri Mar 13 1998 Marc Ewing <marc@redhat.com>

- Added -k, Obsoletes

- Integrate into CVS source tree

%prep
%setup -n imlib
#%patch0 -p1

%build
# Needed for snapshot releases.
export CC="%{cc}"
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)

%doc README

%{prefix}/lib/lib*.so.*
%{prefix}/etc/*
%{prefix}/bin/*

%files devel
%defattr(-, root, root)

%doc HACKING
%doc doc/index.html
%doc doc/bg.gif
%doc doc/border_diag.gif
%doc doc/border_eg1.gif
%doc doc/border_eg2.gif
%doc doc/border_eg3.gif
%doc doc/curve1.gif
%doc doc/curve2.gif
%doc doc/imlib.gif

%{prefix}/lib/lib*.so
%{prefix}/lib/*a
%{prefix}/include/*
