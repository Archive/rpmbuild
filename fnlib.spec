# Note that this is NOT a relocatable package
%define ver       1.1.0
%define rel       980704
%define prefix    /usr
%define buildroot /var 
%define cc        egcs -g

Name: fnlib
Version: %ver
Release: %rel
Copyright: LGPL
Source: fnlib-SNAP-%{rel}.tar.gz
BuildRoot: %{buildroot}/tmp/fnlib-root
Packager: Andreas Kostyrka <andreas@ag.or.at>
URL: http://www.gnome.org
Prereq: /sbin/install-info
Docdir: %{prefix}/doc
Summary: glib of gtk: Handy library of utility functions
Group: Libraries

%description
The fnlib from gnome CVS. Needed for E.

%package devel
Summary: Devel package for fnlib.
Group: Libraries

%description devel
Devel package for fnlib.

%changelog

* Mon Jul 04 1998 Andreas Kostyrka <andreas@ag.or.at>

- cloned from glib.spec
  
%prep
%setup -n fnlib

%build
# Needed for snapshot releases.
export CC="%{cc}"
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%{prefix}/fnlib_fonts/e
%{prefix}/fnlib_fonts/rock
%{prefix}/fnlib_fonts/shinymetal
%{prefix}/etc/fnrc
%{prefix}/lib/libFnlib.so.0.3.0
%{prefix}/lib/libFnlib.so.0
%{prefix}/lib/libFnlib.so

%files devel
%defattr(-, root, root)
%{prefix}/lib/libFnlib.la
%{prefix}/lib/libFnlib.a
%{prefix}/include/Fnlib.h
%{prefix}/include/Fnlib_types.h

