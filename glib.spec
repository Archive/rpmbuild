# Note that this is NOT a relocatable package
%define ver       1.1.0
%define rel       980712
%define prefix    /usr
%define buildroot /var 
%define cc        egcs -g

Name: glib
Version: %ver
Release: %rel
Copyright: LGPL
Source: ftp://ftp.jimpick.com/pub/gnome/snap/glib/glib-SNAP-%{rel}.tar.gz
BuildRoot: %{buildroot}/tmp/glib-root
Packager: Andreas Kostyrka <andreas@ag.or.at>
URL: http://www.gtk.org
Prereq: /sbin/install-info
Docdir: %{prefix}/doc
Summary: glib of gtk: Handy library of utility functions
Group: Libraries


%description
The glib library originally written for gtk.
Handy library of utility functions.  Development libs and headers
are in glib-devel.

%package devel
Summary: glib devel package.
Group: Libraries

%description devel
Static libraries and header files for the glib library.

%changelog

* Mon Jun 22 1998 Andreas Kostyrka <andreas@ag.or.at>

- made seperate spec file for CVS snapshot glib.
  
%prep
%setup -n glib

%build
# Needed for snapshot releases.
export CC="%{cc}"
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-, root, root)
%{prefix}/lib/libglib-1.1.so.0.0.0
%{prefix}/lib/libglib-1.1.so.0
%{prefix}/lib/libglib-1.1.so

%files devel
%defattr(-, root, root)
%{prefix}/lib/libglib-1.1.la
%{prefix}/lib/libglib-1.1.a
%{prefix}/lib/glib
%{prefix}/bin/glib-config
%{prefix}/share/aclocal/glib.m4
%{prefix}/include/glib.h

